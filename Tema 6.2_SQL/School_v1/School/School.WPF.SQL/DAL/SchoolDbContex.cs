﻿using System;
using Microsoft.EntityFrameworkCore;
using School.Lib.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.IU.WPF.DAL
{
    public  class SchoolDbContex : DbContext
    {
            public DbSet<Student> Students { get; set; }
            public DbSet<Subject> Subjects { get; set; }
            public DbSet<Enrollment> Enrollments { get; set; }

        
        public SchoolDbContex() : base()
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder) 
        {
            modelBuilder.Entity<Student>().ToTable("Studens");
            modelBuilder.Entity<Subject>().ToTable("Subjects");
            modelBuilder.Entity<Enrollment>().ToTable("Enrollments");

            modelBuilder.Entity<Enrollment>()
               .HasOne(e => e.Student)
               .WithMany(s => s.Enrollments)
               .HasForeignKey(e => e.StudentId);

            modelBuilder.Entity<Enrollment>()
               .HasOne(e => e.Subject)
               .WithMany(s => s.Enrollments)
               .HasForeignKey(e => e.SubjectId);

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)

        {
            optionsBuilder.UseSqlServer(@"server=DESKTOP-7T1CV5U;Database=Academy;Trusted_Connection=True;MultipleActiveResultSets=true");
            {

            }

        }

    }
}
