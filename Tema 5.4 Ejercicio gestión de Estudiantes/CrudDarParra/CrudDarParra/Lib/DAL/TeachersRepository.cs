﻿using System;
using System.Collections.Generic;
using System.Linq;
using CrudDarParra.Lib.Models;

namespace CrudDarParra.Lib.DAL
{
    public static class TeachersRepositoriy
    {
        private static Dictionary<Guid, Teacher> Teachers
        {
            get
            {

                if (_Teachers == null)
                {
                    //entonces lo inicializo
                    _Teachers = new Dictionary<Guid, Teacher>();

                    var std1 = new Teacher()
                    {
                        Id = Guid.NewGuid(),
                        Name = "Pepe",
                        Email = "p@p.com",
                        Dni = "12345678a"
                    };
                    var std2 = new Teacher()
                    {
                        Id = Guid.NewGuid(),
                        Name = "Marta",
                        Email = "m@m.com",
                        Dni = "12345678b"
                    };

                    _Teachers.Add(std1.Id, std1);
                    _Teachers.Add(std2.Id, std2);
                }

                return _Teachers;
            }
        }
        static Dictionary<Guid, Teacher> _Teachers;

        public static List<Teacher> TeachersList
        {
            get
            {
                return GetAll();
            }
        }

        public static List<Teacher> GetAll()
        {
            
            return Teachers.Values.ToList();
        }

        public static Teacher Get(Guid id)
        {
            // si existe un Teacher con es id en la DB me lo devuelve
            if (Teachers.ContainsKey(id))
                return Teachers[id];
            else
                // si no, me devuelve el Teacher por defecto,
                // prob un null
                return default(Teacher);
        }

        public static Teacher GetByDni(string dni)
        {
            foreach (var item in Teachers)
            {
                var Teacher = item.Value;
                if (Teacher.Dni == dni)
                    return Teacher;
            }

            return default(Teacher);
        }

        public static List<Teacher> GetByName(string name)
        {
            var output = new List<Teacher>();

            foreach (var item in Teachers)
            {
                var Teacher = item.Value;

                if (Teacher.Name == name)
                    output.Add(Teacher);
            }

  
            return output;
        }

        public static TeacherValidationsTypes Add(Teacher Teacher)
        {
            if (Teacher.Id != default(Guid))
            {
                // todo bien porque no hay ningún Id
                return TeacherValidationsTypes.IdNotEmpty;
            }
            else if (!Teacher.ValidateDniFormat(Teacher.Dni))
            {
                //el dni está mal construido
                return TeacherValidationsTypes.WrongDniFormat;
            }
            else
            {
                var stdWithSameDni = GetByDni(Teacher.Dni);
                if (stdWithSameDni != null && Teacher.Id != stdWithSameDni.Id)
                {
                    // hay dos estudiantes distintos con mismo dni
                    return TeacherValidationsTypes.DniDuplicated;
                }
            }

            if (!Teacher.ValidateName(Teacher.Name))
            {
                return TeacherValidationsTypes.WrongNameFormat;
            }
            else if (!Teachers.ContainsKey(Teacher.Id))
            {
                Teacher.Id = Guid.NewGuid();
                Teachers.Add(Teacher.Id, Teacher);

                return TeacherValidationsTypes.Ok;
            }

            // si el Id no estaba vacío y ya existía en la DB devolvmenos false
            return TeacherValidationsTypes.IdDuplicated;
        }

        public static TeacherValidationsTypes Update(Teacher Teacher)
        {
            if (Teacher.Id == default(Guid))
            {
                // no se puede actualizar un registro sin id
                return TeacherValidationsTypes.IdEmpty;
            }
            if (!Teachers.ContainsKey(Teacher.Id))
            {
                // no se puede actualizar un registro
                // que no exista en la DB
                return TeacherValidationsTypes.TeacherNotFound;
            }

            // comprobamos que el dni sea correcto
            if (!Teacher.ValidateDniFormat(Teacher.Dni))
            {
                //el dni está mal construido
                return TeacherValidationsTypes.WrongDniFormat;
            }


            // comprobamos que no haya otro alumno diferente
            // con el mismo dni

            var stdWithSameDni = GetByDni(Teacher.Dni);
            if (stdWithSameDni != null && Teacher.Id != stdWithSameDni.Id)
            {
                // hay dos estudiantes distintos con mismo dni
                return TeacherValidationsTypes.DniDuplicated;
            }

            if (!Teacher.ValidateName(Teacher.Name))
            {
                return TeacherValidationsTypes.WrongNameFormat;
            }

            Teachers[Teacher.Id] = Teacher;

            return TeacherValidationsTypes.Ok;
        }

        public static TeacherValidationsTypes Delete(Guid id)
        {
            if (Teachers.ContainsKey(id))
            {
                Teachers.Remove(id);
                return TeacherValidationsTypes.TeacherNotFound;
            }
            else
                return TeacherValidationsTypes.Ok;
        }

    }
}
