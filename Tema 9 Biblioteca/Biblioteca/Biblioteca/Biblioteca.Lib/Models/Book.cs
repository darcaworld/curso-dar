﻿using System;
using System.Collections.Generic;
using Common.Lib.Authenticator;
using Common.Lib.Core;


namespace Biblioteca.Lib.Models
{
    public class Book : Entity
    {
        #region Static Validations

        public static bool ValidateAutor(string autor)
        {
            if (string.IsNullOrEmpty(autor.Trim()))
            {
                return false;
            }
            return true;
        }

        public static bool ValidateIdFormat(string Id)
        {
            if (string.IsNullOrEmpty(Id))
                return false;
            else if (Id.Length != 10)
                return false;
            else
                return true;

        }
       
        #endregion
        public string Autor { get; set; }
        public string Titulo { get; set; }
        public string CB { get; set; } //codigo book
      

        public virtual List<BookCopy> BookCopies { get; set; }

        public Book()
        {

        }

        public Book Clone()
        {
            var output = new Book
            {
                Id = this.Id,
                Autor = this.Autor,
                Titulo = this.Titulo,
                CB = this.CB, //codigo book
            };

            return output;
        }
    }
    public enum BookValidationsTypes
    {
        Ok,
        WrongIdFormat,
        ValidateIdFormat,
        IdDuplicated,
        WrongAutorFormat,
        IdNotEmpty,
        IdEmpty,
        BookNotFound,
        WrongCBFormat,
        CBDuplicated,
        ValidateCBFormat

    }
}
