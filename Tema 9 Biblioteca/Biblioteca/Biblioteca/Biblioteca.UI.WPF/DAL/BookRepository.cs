﻿using System;
using System.Collections.Generic;
using System.Linq;
using Biblioteca.Lib.Models;

namespace Biblioteca.Lib.DAL
{
    public class BookRepository : IDisposable
    {
        public BibliotecaDbContext DbContext { get; set; }

        public List<Book> BooksList
        {
            get
            {
                return GetAll();
            }
        }

        public BookRepository()
        {
            DbContext = new BibliotecaDbContext();
            LoadInitData();
        }

        public void LoadInitData()
        {
            if (DbContext.Books.Count() == 0)
            {

                var book1 = new Book()
                {
                    Id = Guid.NewGuid(),
                    Autor = "JF",
                    Titulo = "La clases de Jose",
                    CB = "123456789"

                };
                var book2 = new Book()
                {
                    Id = Guid.NewGuid(),
                    Autor = "FJ",
                    Titulo = "c# by Josep",
                    CB = "987654321"

                };

                DbContext.Books.Add(book1);
                DbContext.Books.Add(book2);

                DbContext.SaveChanges();
            }

        }

        public List<Book> GetAll()
        {

            return DbContext.Books.ToList();
        }

        public Book Get(Guid id)
        {
            var output = DbContext.Books.Find(id);
            return output;
        }

        public List<Book> GetByAutor(string Autor)
        {
            var output = DbContext.Books.Where(s => s.Autor == Autor).ToList();
            return output;
        }
        public Book GetByCB(string CB)
        {
            var output = DbContext.Books.FirstOrDefault(s => s.CB == CB);
            return output;

        }
        public BookValidationsTypes Add(Book Book)
        {
            if (Book.Id != default(Guid))
            {

                return BookValidationsTypes.IdNotEmpty;
            }
            else if (!Book.ValidateIdFormat(Book.CB))
            {

                return BookValidationsTypes.WrongCBFormat;
            }
            else
            {
                var stdWithSameCB = GetByCB(Book.CB);
                if (stdWithSameCB != null && Book.Id != stdWithSameCB.Id)
                {

                    return BookValidationsTypes.CBDuplicated;
                }
            }

            if (!Book.ValidateAutor(Book.Autor))
            {
                return BookValidationsTypes.WrongAutorFormat;
            }

            else if (DbContext.Books.All(s => s.Id != Book.Id))
            {
                Book.Id = Guid.NewGuid();
                DbContext.Books.Add(Book);
                DbContext.SaveChanges();

                return BookValidationsTypes.Ok;
            }


            return BookValidationsTypes.IdDuplicated;
        }

        public BookValidationsTypes Update(Book Book)
        {
            if (Book.Id == default(Guid))
            {

                return BookValidationsTypes.IdEmpty;
            }
            if (DbContext.Books.All(s => s.Id != Book.Id))
            {

                return BookValidationsTypes.BookNotFound;
            }


            if (!Book.ValidateIdFormat(Book.CB))
            {

                return BookValidationsTypes.WrongCBFormat;
            }


            var stdWithSameCB = GetByCB(Book.CB);
            if (stdWithSameCB != null && Book.Id != stdWithSameCB.Id)
            {
                return BookValidationsTypes.CBDuplicated;
            }

            if (!Book.ValidateAutor(Book.Autor))
            {
                return BookValidationsTypes.WrongAutorFormat;
            }

            DbContext.Books.Update(Book);
            DbContext.SaveChanges();

            return BookValidationsTypes.Ok;
        }

        public BookValidationsTypes Delete(Guid id)
        {
            var Book = DbContext.Books.Find(id);
            if (Book == null)
            {
                return BookValidationsTypes.BookNotFound;
            }
            else
            {
                DbContext.Books.Remove(Book);
                return BookValidationsTypes.Ok;
            }
        }

        public void Dispose()
        {
        }
    }
}
