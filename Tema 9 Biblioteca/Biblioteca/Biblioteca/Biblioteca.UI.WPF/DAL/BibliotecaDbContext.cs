﻿using Microsoft.EntityFrameworkCore;
using Biblioteca.Lib.Models;
using System;

namespace Biblioteca.Lib.DAL
{
    public class BibliotecaDbContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<BookCopy> BookCopys { get; set; }
        public DbSet<Loan> Loans { get; set; }

        public BibliotecaDbContext() : base()
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>().ToTable("Students");
            modelBuilder.Entity<Book>().ToTable("Books");
            modelBuilder.Entity<BookCopy>().ToTable("BookCopys");
            modelBuilder.Entity<Loan>().ToTable("Loans");


            modelBuilder.Entity<Loan>()
               .HasOne(e => e.Student)
               .WithMany(s => s.Loans)
               .HasForeignKey(e => e.StudentId);

            modelBuilder.Entity<Loan>()
                .HasOne(e => e.BookCopy)
                .WithMany(s => s.Loans)
                .HasForeignKey(e => e.BookCopyId);

            modelBuilder.Entity<BookCopy>()
              .HasOne(e => e.Book)
              .WithMany(s => s.BookCopies)
              .HasForeignKey(e => e.BookId);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(@"Server=LAPTOP-TSPE80VL;Database=Academy;Trusted_Connection=True;MultipleActiveResultSets=true");
            optionsBuilder
                .UseMySql(connectionString: @"server=localhost;database=Academy;uid=DPARRA;password=12345678;",
                new MySqlServerVersion(new Version(8, 0, 23)));
        }
    }
}
