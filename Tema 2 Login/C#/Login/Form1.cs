﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace Login
{
    public partial class frmprincipal : Form
    {
        string ruta;

        public frmprincipal()
        {
            InitializeComponent();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
           Application.Exit();
        }

        private void cerrarSesiónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void abrirdocToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog Archivo = new OpenFileDialog();
            Archivo.Title = "Documentos de word";
            Archivo.Filter = "Documentos de word(*.doc, *.docx)|*.doc; *.docx";

            if (Archivo.ShowDialog() == DialogResult.OK)
            {
                ruta = Archivo.FileName;
                Process.Start(ruta);

            }

        }
    }
}
