﻿using Objetos.Lib.Models;
using System;

namespace Objetos.Lib.Models
{
    public class Exam : Entity
    {
        #region Satic Validations

        public static string MarkFormatError = "La nota está vacía o en un formato incorrecto";
        public static string MarkRangeError = "La nota debe estar entre 0 y 10";

        public static bool ValidateMarkRange(double input)
        {
            return (0.0 <= input) && (input <= 10.0);
        }

        #endregion

        public Student Student { get; set; }
        public Subject Subject { get; set; }
        public double Mark { get; set; }

        public DateTime TimeStamp { get; set; }
    }
}