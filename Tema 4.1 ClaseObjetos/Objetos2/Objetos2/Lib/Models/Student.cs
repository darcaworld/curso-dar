﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Objetos.Lib.Models
{
    public class Student : Entity
    {
        #region Satic Validations

        public static string DniFormatError = "El dni está en un formato incorrecto";
        public static string DniDuplicated = "Ya existe un estudiante con el dni";
        public static string NameFormatError = "El nombre está vacío o en un formato incorrecto";

        public static bool IsDniValid(string dni)
        {
            if (!ValidateDniFormat(dni))
                return false;

            return Program.Students.Values.Any(x => x.Dni == dni);
        }
        public static bool ValidateDniFormat(string input)
        {
            return !(string.IsNullOrEmpty(input) || input.Length != 9);
        }

        public static bool ValidateDniDuplicated(string input, string currentDni = "")
        {
            if (string.IsNullOrEmpty(currentDni) || (input != currentDni))
            {
                return !Program.Students.Values.Any(x => x.Dni == input);
            }
            return true;
        }

        public static bool ValidateNameFormat(string input)
        {
            return !string.IsNullOrEmpty(input);
        }

        #endregion

        public string Name { get; set; }

        public string Dni { get; set; }

        public List<Exam> Exams { get; set; }

        public Student()
        {
            Exams = new List<Exam>();
        }

        public bool AddExam(Exam exam)
        {
            exam.Student = this;
            Exams.Add(exam);

            return true;
        }
    }
}
