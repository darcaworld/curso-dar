﻿using System;
using System.Linq;

namespace ConsoleApp1.Lib.Models
{
    public class Subject : Entity
    {
        #region Static Validations

        public static string NameFormatError = "El nombre de la asignatura está en un formato incorrecto";
        public static string NameDuplicated = "Ya existe una asignatura con el nombre";

        public static bool IsNameValid(string text)
        {
            if (!ValidateNameFormat(text))
                return false;

            return Program.Subjects.Values.Any(x => x.Name == text);
        }

        public static bool ValidateNameFormat(string input)
        {
            return !(string.IsNullOrEmpty(input));
        }

        public static bool ValidateNameDuplicated(string input, string currentName = "")
        {
            if (string.IsNullOrEmpty(currentName) || (input != currentName))
            {
                return !Program.Subjects.Values.Any(x => x.Name == input);
            }
            return true;
        }
        #endregion

        public string Name { get; set; }

        public string Teacher { get; set; }


    }
}
