﻿
namespace CrudDarParra.Lib.Models
{
        public class Teacher : Entity
        {
            #region Static Validations

            public static bool ValidateDniFormat(string dni)
            {
                if (string.IsNullOrEmpty(dni))
                    return false;
                else if (dni.Length != 9)
                    return false;
                else
                    return true;
            }

            public static bool ValidateName(string name)
            {
                if (string.IsNullOrEmpty(name.Trim()))
                {
                   
                    return false;
                }
                return true;
            }

            #endregion

            public string Dni { get; set; }
            public string Name { get; set; }
            public string Email { get; set; }

            public Teacher()
            {
            }

            public Teacher Clone()
            {
                var output = new Teacher
                {
                    Id = this.Id,
                    Dni = this.Dni,
                    Name = this.Name,
                    Email = this.Email
                };

                return output;
            }
        }

        public enum TeacherValidationsTypes
        {
            Ok,
            WrongDniFormat,
            DniDuplicated,
            WrongNameFormat,
            IdNotEmpty,
            IdDuplicated,
            IdEmpty,
            TeacherNotFound
        }

}
