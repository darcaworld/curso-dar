// Updated by XamlIntelliSenseFileGenerator 11/7/2021 1:45:11 PM
#pragma checksum "..\..\..\..\Views\Students.View.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5EAF877A2D089E52A90EF6B3EE9353B9272A97E5"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using GDE.Views;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace GDE.Views
{


    /// <summary>
    /// StudentsView
    /// </summary>
    public partial class StudentsView : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector
    {

#line default
#line hidden

        private bool _contentLoaded;

        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "6.0.0.0")]
        public void InitializeComponent()
        {
            if (_contentLoaded)
            {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/GDE;V1.0.0.0;component/views/students.view.xaml", System.UriKind.Relative);

#line 1 "..\..\..\..\Views\Students.View.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);

#line default
#line hidden
        }

        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "6.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target)
        {
            switch (connectionId)
            {
                case 1:
                    this.TbDni = ((System.Windows.Controls.TextBox)(target));

#line 23 "..\..\..\..\Views\Students.View.xaml"
                    this.TbDni.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TbDni_TextChanged);

#line default
#line hidden
                    return;
                case 2:
                    this.TbName = ((System.Windows.Controls.TextBox)(target));

#line 26 "..\..\..\..\Views\Students.View.xaml"
                    this.TbName.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TbName_TextChanged);

#line default
#line hidden
                    return;
                case 3:
                    this.TbEmail = ((System.Windows.Controls.TextBox)(target));

#line 29 "..\..\..\..\Views\Students.View.xaml"
                    this.TbEmail.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TbEmail_TextChanged);

#line default
#line hidden
                    return;
                case 4:

#line 38 "..\..\..\..\Views\Students.View.xaml"
                    ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click);

#line default
#line hidden
                    return;
                case 5:

#line 39 "..\..\..\..\Views\Students.View.xaml"
                    ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click_1);

#line default
#line hidden
                    return;
                case 6:
                    this.BtSave = ((System.Windows.Controls.Button)(target));

#line 40 "..\..\..\..\Views\Students.View.xaml"
                    this.BtSave.Click += new System.Windows.RoutedEventHandler(this.BtSave_Click);

#line default
#line hidden
                    return;
                case 7:
                    this.DgStudents = ((System.Windows.Controls.DataGrid)(target));
                    return;
            }
            this._contentLoaded = true;
        }

        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "6.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target)
        {
            switch (connectionId)
            {
                case 8:

#line 48 "..\..\..\..\Views\Students.View.xaml"
                    ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.BtSelect_Click);

#line default
#line hidden
                    break;
                case 9:

#line 55 "..\..\..\..\Views\Students.View.xaml"
                    ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.BtDelete_Click);

#line default
#line hidden
                    break;
            }
        }
    }
}

