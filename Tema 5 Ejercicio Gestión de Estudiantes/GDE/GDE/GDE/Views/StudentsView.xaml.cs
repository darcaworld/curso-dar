﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GDE.lib.DAL;
using GDE.lib.Models;

namespace GDE.Views
{
    public partial class StudentsView : UserControl
    {
        public Student SelectedStudent
        {
            get
            {
                return _selectedStudent;
            }
            set
            {
                _selectedStudent = value;
                if (value == null)
                {
                    BtSave.Visibility = Visibility.Hidden;

                    TbDni.Text = string.Empty;
                    TbName.Text = string.Empty;
                    TbEmail.Text = string.Empty;
                }
                else
                {
                    BtSave.Visibility = Visibility.Visible;

                    TbDni.Text = value.Dni;
                    TbName.Text = value.Name;
                    TbEmail.Text = value.Email;
                }
            }
        }
        Student _selectedStudent;
        public StudentsView()
        {
            InitializeComponent();

            DgStudents.ItemsSource = StudentsRepository.GetAll();


            var dict = new Dictionary<Guid, Student>();
            var list = new List<Student>();

            foreach (var item in dict)
            {
                var key = item.Key;
                var student = item.Value;
                Console.WriteLine(student.Name);
            }


            foreach (var student in list)
            {
                Console.WriteLine(student.Name);
            }

        }

        public void Clear()
        {
            SelectedStudent = null;
        }

        public void Create()
        {
            var student = new Student
            {
                Name = TbName.Text,
                Email = TbEmail.Text,
                Dni = TbDni.Text
            };

          

            var addResult = StudentsRepository.Add(student);
            if (addResult != StudentValidationsTypes.OK)
                MessageBox.Show($"error adding student:{addResult}");
            else
                DgStudents.ItemsSource = StudentsRepository.GetAll();

            Clear();
        }

        public void Retrieve()
        {
    

            var allStudents = StudentsRepository.GetAll();


        
            var idAleatorio = Guid.NewGuid();
            var student = StudentsRepository.Get(idAleatorio);

            var student2 = StudentsRepository.GetByDni("12345678a");

        }

        public void Update()
        {
            if (SelectedStudent != null)
            {
                var studentCopy = SelectedStudent.Clone();

                studentCopy.Dni = TbDni.Text;
                studentCopy.Name = TbName.Text;
                studentCopy.Email = TbEmail.Text;

                var editResult = StudentsRepository.Update(studentCopy);
                if (editResult != StudentValidationsTypes.OK)
                    MessageBox.Show($"error editing student:{editResult}");
                else
                    DgStudents.ItemsSource = StudentsRepository.GetAll();
            }
        }

        public void Delete(Student student)
        {
            if (student != null)
            {
                var deleteResult = StudentsRepository.Delete(student.Id);
                if (deleteResult != StudentValidationsTypes.OK)
                    MessageBox.Show($"error deleting student:{deleteResult}");
                else
                    DgStudents.ItemsSource = StudentsRepository.GetAll();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
