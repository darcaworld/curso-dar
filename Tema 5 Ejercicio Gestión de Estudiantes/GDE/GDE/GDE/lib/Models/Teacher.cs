﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDE.lib.Models
{
    public class Teacher : Entity
    {
        #region Static Validations

        public static bool ValidateDniFormat(string Id)
        {
            if (string.IsNullOrEmpty(Id))
                return false;
            else if (Id.Length != 5)
                return false;
            else
                return true;
        }

        public static bool ValidateName(string name)
        {
            if (string.IsNullOrEmpty(name.Trim()))
            {
               
                return false;
            }
            return true;
        }

        #endregion
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Horario {  get; set; }

        public Teacher()
        {

        }

        public Teacher Clone()
        {
            var output = new Teacher
            {
                Id = this.Id,
                Name = this.Name,
                Email = this.Email,
                Horario = this.Horario

            };
            return output;
        }

    }

    public enum TeacherValidationsTypes
    {

        OK,
        ALERT_Horario_Format,
        ALERT_Name_Format,
        Id_NO_Empty,
        Id_Duplicated,
        Id_Empty,
        Teacher_NOT_Found
    }
}

