﻿using System;
using GDE.lib.Models;
using System.Collections.Generic;
using System.Linq;


namespace GDE.lib.DAL
{
    public class StudentsRepository
    {
        private static Dictionary<Guid, Student> Students
        {
            get
            {
                if (_Students == null)
                {
                    _Students = new Dictionary<Guid, Student>();

                    var std1 = new Student()
                    {
                        Id = Guid.NewGuid(),
                        Name = "PARRA_Dar",
                        Email = "dp@dp.com",
                        Dni = "12345678A"
                    };

                    var std2 = new Student()
                    {
                        Id = Guid.NewGuid(),
                        Name = "Jose",
                        Email = "j@m.com",
                        Dni = "12345678B"
                    };

                    _Students.Add(std1.Id, std1);
                    _Students.Add(std2.Id, std2);
                }
                return _Students;

            }
        }
        static Dictionary<Guid, Student> _Students;

        public static List<Student> StudentsList
        {

            get
            {

                return GetAll();
            }
        }

        public static List<Student> GetAll()
        {

            return Students.Values.ToList();

        }

        public static Student Get(Guid id)
        {
            if (Students.ContainsKey(id))
                return Students[id];
            else
                return default(Student);
        }
        
        public static Student GetByDni(string dni)
        {
            foreach (var item in Students)
            {
                var Student = item.Value;
                if (Student.Dni == dni)
                    return Student;
            }
            return default (Student);
        }
        
        public static List<Student> GetByName(string name)
        {
            var output = new List<Student>();

            foreach (var item in Students)
            {
                var student = item.Value;

                if (student.Name == name)
                    output.Add(student);
            }
            return output;
        }

        public static StudentValidationsTypes Add(Student student)
        {
            if (student.Id != default(Guid))
            {

                return StudentValidationsTypes.Id_NO_Empty;
            }
            else if (!Student.ValidateDniFormat(student.Dni))
            {
                return StudentValidationsTypes.ALERT_Dni_Format;
;
            }
            else
            {
                var stdWithSameDni = GetByDni (student.Dni);
                if (stdWithSameDni != null && student.Id != stdWithSameDni.Id)
                {
                    return StudentValidationsTypes.Dni_Duplicate;
                }
            }

            if (!Student.ValidateName(student.Name))
            {
                return StudentValidationsTypes.ALERT_Name_Format;
            }
            else if (!Students.ContainsKey(student.Id))
            {
                student.Id = Guid.NewGuid();
                Students.Add(student.Id, student);

                return StudentValidationsTypes.OK;
            }

            return StudentValidationsTypes.Id_Duplicated;
        }

        public static StudentValidationsTypes Update(Student student)
        {
            if (student.Id == default(Guid))
            {
                return StudentValidationsTypes.Id_Empty;
            }

            if (!Students.ContainsKey(student.Id))
            {
                return StudentValidationsTypes.Student_NOT_Found;
            }
            if (!Student.ValidateDniFormat(student.Dni))
            {

                return StudentValidationsTypes.ALERT_Dni_Format;
            }
            var stdWithSameDni = GetByDni(student.Dni);
            if (stdWithSameDni != null && student.Id != stdWithSameDni.Id)
            {
                return StudentValidationsTypes.Dni_Duplicate;
            } 
            if (!Student.ValidateName(student.Name))
            {
                return StudentValidationsTypes.ALERT_Name_Format;
            }

            Students [student.Id] = student;

            return StudentValidationsTypes.OK;
        }
        public static StudentValidationsTypes Delete(Guid id)
        {
            if (Students.ContainsKey(id))
            {
                Students.Remove(id);
                return StudentValidationsTypes.Student_NOT_Found;
            }
            else
                return StudentValidationsTypes.OK;
        }
        

    }
}
