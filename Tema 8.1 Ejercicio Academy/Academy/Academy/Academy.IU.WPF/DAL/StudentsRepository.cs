﻿using System;
using System.Collections.Generic;
using System.Linq;
using Academy.Lib.Models;

namespace Academy.Lib.DAL
{
    public class StudentsRepository : IDisposable
    {
        public AcademyDbContext DbContext { get; set; }

        public List<Student> StudentsList
        {
            get
            {
                return GetAll();
            }
        }

        public StudentsRepository()
        {
            DbContext = new AcademyDbContext();
            LoadInitData();
        }

        public void LoadInitData()
        {
            if (DbContext.Students.Count() == 0)
            {
             
                var std1 = new Student()
                {
                    Id = Guid.NewGuid(),
                    Name = "Pepe",
                    Email = "p@p.com",
                    Dni = "12345678a"
                };
                var std2 = new Student()
                {
                    Id = Guid.NewGuid(),
                    Name = "Marta",
                    Email = "m@m.com",
                    Dni = "12345678b"
                };

                DbContext.Students.Add(std1);
                DbContext.Students.Add(std2);

                DbContext.SaveChanges();
            }

        }

        public List<Student> GetAll()
        {
          
            return DbContext.Students.ToList();
        }

        public Student Get(Guid id)
        {
           
            var output = DbContext.Students.Find(id);
            return output;
        }

        public Student GetByDni(string dni)
        {
           
            var output = DbContext.Students.FirstOrDefault(s => s.Dni == dni);
            return output;
        }

        public List<Student> GetByName(string name)
        {
           

            var output = DbContext.Students.Where(s => s.Name == name).ToList();
            return output;
        }

        public StudentValidationsTypes Add(Student student)
        {
            if (student.Id != default(Guid))
            {
                
                return StudentValidationsTypes.IdNotEmpty;
            }
            else if (!Student.ValidateDniFormat(student.Dni))
            {
                
                return StudentValidationsTypes.WrongDniFormat;
            }
            else
            {
                var stdWithSameDni = GetByDni(student.Dni);
                if (stdWithSameDni != null && student.Id != stdWithSameDni.Id)
                {
                    
                    return StudentValidationsTypes.DniDuplicated;
                }
            }

            if (!Student.ValidateName(student.Name))
            {
                return StudentValidationsTypes.WrongNameFormat;
            }
           
            else if (DbContext.Students.All(s => s.Id != student.Id)) 
            {
                student.Id = Guid.NewGuid();
                DbContext.Students.Add(student);
                DbContext.SaveChanges();

                return StudentValidationsTypes.Ok;
            }

           
            return StudentValidationsTypes.IdDuplicated;
        }

        public StudentValidationsTypes Update(Student student)
        {
            if (student.Id == default(Guid))
            {
               
                return StudentValidationsTypes.IdEmpty;
            }
            if (DbContext.Students.All(s => s.Id != student.Id))
            {
                
                return StudentValidationsTypes.StudentNotFound;
            }

            
            if (!Student.ValidateDniFormat(student.Dni))
            {
                
                return StudentValidationsTypes.WrongDniFormat;
            }


            var stdWithSameDni = GetByDni(student.Dni);
            if (stdWithSameDni != null && student.Id != stdWithSameDni.Id)
            { 
                return StudentValidationsTypes.DniDuplicated;
            }

            if (!Student.ValidateName(student.Name))
            {
                return StudentValidationsTypes.WrongNameFormat;
            }

            DbContext.Students.Update(student);
            DbContext.SaveChanges();

            return StudentValidationsTypes.Ok;
        }

        public StudentValidationsTypes Delete(Guid id)
        {
            var student = DbContext.Students.Find(id);
            if (student == null)
            {
                return StudentValidationsTypes.StudentNotFound;
            }
            else
            {
                DbContext.Students.Remove(student);
                return StudentValidationsTypes.Ok;
            }
        }

        public void Dispose()
        {
        }
    }
}
