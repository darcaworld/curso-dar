﻿using System;

namespace Objetos.Lib.Models
{
    public class Exam : Entity
    {
        public Student Student {  get; set; }
        public Subject Subject {  get; set; }
        public double Mark {  get; set; }

    }
}
