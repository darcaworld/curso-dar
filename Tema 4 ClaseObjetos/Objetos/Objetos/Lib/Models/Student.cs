﻿using System.Collections.Generic;

namespace Objetos.Lib.Models
{
    public class Student : Entity
    {
        public string Name {  get; set; }
        public string DNI {  get; set; }

        public List<Exam> Exams {  get; set; }
        public static IEnumerable<object> Values { get; internal set; }

        public Student()
        {

            Exams = new List<Exam>();
        }

        public bool AddExam(Exam exam)
        { 
        exam.Student = this;
            return true;
        
        }
    }
}
