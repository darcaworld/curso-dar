﻿using Objetos.Lib.Models;
using System;
using System.Collections.Generic;

namespace Objetos
{
    class program
    {

        //static List<double> Marks {  get; set; }
        
        public static Dictionary<string, Student> Students = new Dictionary<string, Student>();

        static string EscapeWord = "SalirDelPrograma";

        static void Main(string[] args)
        {
            Console.WriteLine("Bienvenidos al programa para gestión de alumnos");
            Console.WriteLine("Para introducir notas de los alumnos use la opción a");
            Console.WriteLine("Para obtener estadístics use la opción e");

            //Marks = new List<double>();
            var keepdoing = true;

            while (keepdoing)
            {
                var option = Console.ReadKey().KeyChar;
                
                if (option == 'a')
                {
                    ShowStudentsMenu();
                }
                else if (option == 'e')
                {

                    ShowStatsMenu();
                }

            }

        }

        static void ShowStudentsMenu()
        {
            Console.WriteLine();
            Console.WriteLine("--Menú de Notas--");

            Console.WriteLine("Para ver todos los estudiantes escriba all");
            Console.WriteLine("Para añadir un nuevo estudiante escriba add");
            Console.WriteLine("Para editar un estudiante escriba edit + el DNI");
            Console.WriteLine("Para borrar un estudiante escriba edit + el DNI");
            Console.WriteLine("Para volver al menú principal escriba m");

            var keepdoing = true;
            while (keepdoing)
            {
                var text = Console.ReadLine();

                switch (text)
                {
                    case "all":
                        ShowStudentsMenu();
                        break;

                    case "add":
                        AddNewStudent();
                        break;

                    case "m":
                        keepdoing = false;
                        break;

                    default:
                        AddMark (text);
                        break;
                }

            }
        }

        private static void ShowMainMenu()
        { }
        static void ShowAllStudents ()
        {
          foreach (var student in Students.Values)
          {
                Console.WriteLine($"{student.DNI} {student.Name}");
          }

        }
        static void AddNewStudent()
        {
    
            Console.WriteLine("Inserte DNI o escriba anular para interrumpir");

            var keepdoing = true;
            while(keepdoing)
            { 
                var DNI = Console.ReadLine();   
                if (DNI == "anular")
                {
                    keepdoing = false;
                    break; 
                }

                else if(string.IsNullOrEmpty(DNI) || DNI.Length != 9) 
                {
                    Console.WriteLine("No es válido un DNI vacío" );
                }
                else if(Students.ContainsKey(DNI))
                {
                    Console.WriteLine($"Dato incorrecto- DNI duplicado {DNI}");
                }
                else
                {
                    while (true)
                    {
                            var Name = Console.ReadLine();

                            if (Name == "anular")
                            {
                                keepdoing = false;
                                break;
                            }
                            if (string.IsNullOrEmpty(Name))
                            {
                                Console.WriteLine("El Nombre esta vacío");
                            }
                                else
                                {
                                    var student = new Student
                                    {
                                        Id = Guid.NewGuid(),
                                        DNI = DNI,
                                        Name = Name,
                                    };
                                    Students.Add(student.DNI, student);
                                    keepdoing = false; 
                                    break;
                                }
                       
                    }
                   

                }

            }

        }
        static void AddMark(string text)
        {
            var mark = 0.0;
            if(double.TryParse(text, out mark))
            {
                //Marks.Add(mark);
                Console.WriteLine("Nota OK, añada Otra nota- precione volver");
            }
            else

            {
                Console.WriteLine($"La Nota introducida {text} no está en formato");
            }
        }

        static void ShowStatsMenu()
        {
            Console.WriteLine();
            Console.WriteLine("---Menu de Estadísticas--");

            Console.WriteLine("Para ver la media escriba avg");
            Console.WriteLine("Para ver la nota más alta escriba max");
            Console.WriteLine("Para ver la nota más baja escriba min");
            Console.WriteLine("Para volver al menú principal escriba m");

            var keepdoing = true;
            while (keepdoing)
            {
                var text = Console.ReadLine();

                switch (text)
                {
                    case "avg":
                        ShowAverage();
                        break;

                    case "max":
                        ShowMaxium();
                        break;

                    case "min":
                        ShowMinium();
                        break;
                    default:

                        Console.WriteLine("Comando no reconocido, introduzca una opción valida");
                        break;
                }

            }

            ShowMainMenu();
        }

        static void ShowAverage()
        {
            //var suma = 0.0;
            //for (var i = <Marks.Count; i++)
            //{
                //suma += Marks[i];
            //}
            //var average = suma / Marks.Count;
            //console.WrileLine("La media de los exámenes es: {0}", average);
        }

        static void ShowMaxium()
        {
            //var max = 0.0;
            //for (var i = 0; i <Marks.Count; i ++)
            //{
            //    if (Marks[i] > max)
            //        max = Marks[i];
            //    Console.WriteLine("La Nota más alta es: {0}", max);
            //}
        }

        static void ShowMinium()
        {
            //var min = 0.0;
            //if (Marck.Count == 0)
            //    min = 0.0;
            //else
            //    min = Marks[0];

            //var min = Marks.Count == 0 ? 0.0 : Marks[0];

            //for (var i = 0; i < Marks.Count; i++)
            //{
            //    if (Marks[i] < min)
            //        min = Marks[i];
            //}
            //Console.WriteLine("La nota mas baja es: {0}", mim);
        
        }

    }

}