﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EjemploCrud.Lib.Models;

namespace EjemploCrud.Lib.DAL
{
    public static class TeachersRepository
    {
        private static Dictionary<Guid, Teacher> teachers
        {
            get
            {
                
                if (_Teachers == null)
                {
                    _Teachers = new Dictionary<Guid, Teacher>();

                    var tea1 = new Teacher()
                    {
                        Id = Guid.NewGuid(),
                        Name = "Josep",
                        Email = "j@f.com",
                        Dni = "23456789a"
                    };
                    var tea2 = new Teacher()
                    {
                        Id = Guid.NewGuid(),
                        Name = "Andrea",
                        Email = "a@fds.com",
                        Dni = "23456789b"
                    };

                    _Teachers.Add(tea1.Id, tea1);
                    _Teachers.Add(tea2.Id, tea2);
                }
                return _Teachers;
            }
        }
        static Dictionary<Guid, Teacher> _Teachers;
        public static List<Teacher> TeachersList
        {
            get
            {
                return GetAll();
            }
        }
        public static List<Teacher> GetAll()
        {
            return Teacher.Values.ToList();
        }
        public static Teacher Get(Guid Id)
        {
           
            if (Teacher.ContainsKey(Id))
                return teacher [Id];
            else     
                return default(Teacher);
        }
        public static Teacher GetByDni(string Id)
        {
            foreach (var item in Teachers)
            {
                var teacher = item.Value;
                if (teacher.Id == id)
                    return teacher;
            }

            return default(Teacher);
        }
        public static List<Teacher> GetByName(string name)
        {
            var output = new List<Teacher>();

            foreach (var item in teachers)
            {
                var teacher = item.Value;

                if (teacher.Name == name)
                    output.Add(teacher);
            }
            return output;
        }
        public static TeacherValidationsTypes Add(Teacher teacher)
        {
            if (teacher.Id != default(Guid))
            {
               
                return TeacherValidationsTypes.IdNotEmpty;
            }
            else if (!Teacher.ValidateDniFormat(teacher.Dni))
            {
              
                return TeacherValidationsTypes.WrongDniFormat;
            }
            else
            {
                var teaWithSameId = GetById (teacher.Id);
                if (teaWithSameId != null && teacher.Id != teaWithSameId.Id)
                {
                    return TeacherValidationsTypes.IdDuplicated;
                }
            }

            if (!Teacher.ValidateName(teacher.Name))
            {
                return TeacherValidationsTypes.WrongNameFormat;
            }
            else if (!Teacher.ContainsKey(teacher.Id)) 
            {
                teacher.Id = Guid.NewGuid();
                teachers.Add(teacher.Id, teacher);

                return TeacherValidationsTypes.Ok;
            }
            return TeacherValidationsTypes.IdDuplicated;
        }
        public static TeacherValidationsTypes Delete(Guid id)
        {
            if (teachers.ContainsKey(id))
            {
                teachers.Remove(id);
                return TeacherValidationsTypes.StudentNotFound;
            }
            else
                return TeacherValidationsTypes.Ok;
        }

    }


}

