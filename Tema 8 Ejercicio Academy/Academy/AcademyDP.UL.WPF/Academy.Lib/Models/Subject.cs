﻿using System;
using Common.Lib.Core;
using System.Collections.Generic;
using System.Text;

namespace Academy.Lib.Models
{
    public class Subject : Entity
    {
        public string Name { get; set; }
        public string code { get; set; }

        public virtual List<Enrollment> Enrrollments { get; set; }

        public Subject ()
        {

        }
    }
}
