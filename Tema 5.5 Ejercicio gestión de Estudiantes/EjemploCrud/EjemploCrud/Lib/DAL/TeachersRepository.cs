﻿using System;
using System.Collections.Generic;
using System.Linq;
using EjemploCrud.Lib.Models;

namespace EjemploCrud.Lib.DAL
{
    public static class TeachersRepository
    {
        private static Dictionary<Guid, Teacher> Teachers
        {
            get
            {
               
                if (_Teachers == null)
                {
                  
                    _Teachers = new Dictionary<Guid, Teacher>();

                    var std1 = new Teacher()
                    {
                        Id = Guid.NewGuid(),
                        Name = "Pepe",
                        Email = "p@p.com",
                        Dni = "12345678a"
                    };
                    var std2 = new Teacher()
                    {
                        Id = Guid.NewGuid(),
                        Name = "Marta",
                        Email = "m@m.com",
                        Dni = "12345678b"
                    };

                    _Teachers.Add(std1.Id, std1);
                    _Teachers.Add(std2.Id, std2);
                }


                return _Teachers;
            }
        }
        static Dictionary<Guid, Teacher> _Teachers;

        public static List<Teacher> TeachersList
        {
            get
            {
                return GetAll();
            }
        }

        public static List<Teacher> GetAll()
        {
            
            return Teachers.Values.ToList();
        }

        public static Teacher Get(Guid id)
        {
           
            if (Teachers.ContainsKey(id))
                return Teachers[id];
            else

                return default(Teacher);
        }

        public static Teacher GetByDni(string dni)
        {
            foreach (var item in Teachers)
            {
                var Teacher = item.Value;
                if (Teacher.Dni == dni)
                    return Teacher;
            }

            return default(Teacher);
        }

        public static List<Teacher> GetByName(string name)
        {
            var output = new List<Teacher>();

            foreach (var item in Teachers)
            {
                var Teacher = item.Value;

                if (Teacher.Name == name)
                    output.Add(Teacher);
            }


            return output;
        }

        public static TeacherValidationsTypes Add(Teacher Teacher)
        {
            if (Teacher.Id != default(Guid))
            {

                return TeacherValidationsTypes.IdNotEmpty;
            }
            else if (!Teacher.ValidateDniFormat(Teacher.Dni))
            {

                return TeacherValidationsTypes.WrongDniFormat;
            }
            else
            {
                var stdWithSameDni = GetByDni(Teacher.Dni);
                if (stdWithSameDni != null && Teacher.Id != stdWithSameDni.Id)
                {

                    return TeacherValidationsTypes.DniDuplicated;
                }
            }

            if (!Teacher.ValidateName(Teacher.Name))
            {
                return TeacherValidationsTypes.WrongNameFormat;
            }
            else if (!Teachers.ContainsKey(Teacher.Id))
            {
                Teacher.Id = Guid.NewGuid();
                Teachers.Add(Teacher.Id, Teacher);

                return TeacherValidationsTypes.Ok;
            }


            return TeacherValidationsTypes.IdDuplicated;
        }

        public static TeacherValidationsTypes Update(Teacher Teacher)
        {
            if (Teacher.Id == default(Guid))
            {

                return TeacherValidationsTypes.IdEmpty;
            }
            if (!Teachers.ContainsKey(Teacher.Id))
            {

                return TeacherValidationsTypes.TeacherNotFound;
            }

            if (!Teacher.ValidateDniFormat(Teacher.Dni))
            {
    
                return TeacherValidationsTypes.WrongDniFormat;
            }

            var stdWithSameDni = GetByDni(Teacher.Dni);
            if (stdWithSameDni != null && Teacher.Id != stdWithSameDni.Id)
            {
              
                return TeacherValidationsTypes.DniDuplicated;
            }

            if (!Teacher.ValidateName(Teacher.Name))
            {
                return TeacherValidationsTypes.WrongNameFormat;
            }

            Teachers[Teacher.Id] = Teacher;

            return TeacherValidationsTypes.Ok;
        }

        public static TeacherValidationsTypes Delete(Guid id)
        {
            if (Teachers.ContainsKey(id))
            {
                Teachers.Remove(id);
                return TeacherValidationsTypes.TeacherNotFound;
            }
            else
                return TeacherValidationsTypes.Ok;
        }

    }
}
