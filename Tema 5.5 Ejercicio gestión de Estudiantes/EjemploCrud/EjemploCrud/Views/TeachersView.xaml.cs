﻿using EjemploCrud.Lib.DAL;
using EjemploCrud.Lib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EjemploCrud.Views
{
    /// <summary>
    /// Lógica de interacción para TeachersView.xaml
    /// </summary>
    public partial class TeachersView : UserControl
    {
        public Teacher SelectedTeacher
        {
            get
            {
                return _selectedTeacher;
            }
            set
            {
                _selectedTeacher = value;
                if (value == null)
                {
                    BtSave.Visibility = Visibility.Hidden;

                    TbDni.Text = string.Empty;
                    TbName.Text = string.Empty;
                    TbEmail.Text = string.Empty;
                }
                else
                {
                    BtSave.Visibility = Visibility.Visible;

                    TbDni.Text = value.Dni;
                    TbName.Text = value.Name;
                    TbEmail.Text = value.Email;
                }
            }
        }
        Teacher _selectedTeacher;

        public TeachersView()
        {
            InitializeComponent();

            DgTeachers.ItemsSource = TeachersRepository.GetAll();

            var dict = new Dictionary<Guid, Teacher>();
            var list = new List<Teacher>();

      
            foreach (var item in dict)
            {
                var key = item.Key;
                var Teacher = item.Value;
                Console.WriteLine(Teacher.Name);
            }

            // así recorremos una lista
            foreach (var Teacher in list)
            {
                // ya tengo el Teacher
                Console.WriteLine(Teacher.Name);
            }

        }

        public void Clear()
        {
            SelectedTeacher = null;
        }

        public void Create()
        {
            var Teacher = new Teacher
            {
                Name = TbName.Text,
                Email = TbEmail.Text,
                Dni = TbDni.Text
            };

            var addResult = TeachersRepository.Add(Teacher);
            if (addResult != TeacherValidationsTypes.Ok)
                MessageBox.Show($"error adding Teacher:{addResult}");
            else
                DgTeachers.ItemsSource = TeachersRepository.GetAll();

            Clear();
        }

        public void Retrieve()
        {


            var allTeachers = TeachersRepository.GetAll();

            var idAleatorio = Guid.NewGuid();
            var Teacher = TeachersRepository.Get(idAleatorio);

            var Teacher2 = TeachersRepository.GetByDni("12345678a");

        }

        public void Update()
        {
            if (SelectedTeacher != null)
            {
                var TeacherCopy = SelectedTeacher.Clone();

                TeacherCopy.Dni = TbDni.Text;
                TeacherCopy.Name = TbName.Text;
                TeacherCopy.Email = TbEmail.Text;

                var editResult = TeachersRepository.Update(TeacherCopy);
                if (editResult != TeacherValidationsTypes.Ok)
                    MessageBox.Show($"error editing Teacher:{editResult}");
                else
                    DgTeachers.ItemsSource = TeachersRepository.GetAll();
            }
        }

        public void Delete(Teacher Teacher)
        {
            if (Teacher != null)
            {
                var deleteResult = TeachersRepository.Delete(Teacher.Id);
                if (deleteResult != TeacherValidationsTypes.Ok)
                    MessageBox.Show($"error deleting Teacher:{deleteResult}");
                else
                    DgTeachers.ItemsSource = TeachersRepository.GetAll();
            }
        }

        private void BtAdd_Click(object sender, RoutedEventArgs e)
        {
            Create();
        }

        private void BtSelect_Click(object sender, RoutedEventArgs e)
        {
            SelectedTeacher = ((Button)sender).DataContext as Teacher;
        }

        private void BtClear_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void BtSave_Click(object sender, RoutedEventArgs e)
        {
            Update();
            SelectedTeacher = null;
        }

        private void BtDelete_Click(object sender, RoutedEventArgs e)
        {
            var selectedTeacher = ((Button)sender).DataContext as Teacher;
            Delete(selectedTeacher);
        }
    }
}
