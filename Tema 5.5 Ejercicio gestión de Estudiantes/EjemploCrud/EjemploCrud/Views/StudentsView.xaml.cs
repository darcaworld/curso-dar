﻿using EjemploCrud.Lib.DAL;
using EjemploCrud.Lib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EjemploCrud.Views
{

    public partial class StudentsView : UserControl
    {
        public Student SelectedStudent
        {
            get
            {
                return _selectedStudent;
            }
            set
            {
                _selectedStudent = value;
                if (value == null)
                {
                    BtSave.Visibility = Visibility.Hidden;

                    TbDni.Text = string.Empty;
                    TbName.Text = string.Empty;
                    TbEmail.Text = string.Empty;
                }
                else
                {
                    BtSave.Visibility = Visibility.Visible;

                    TbDni.Text = value.Dni;
                    TbName.Text = value.Name;
                    TbEmail.Text = value.Email;
                }
            }
        }
        Student _selectedStudent;

        public StudentsView()
        {
            InitializeComponent();


            DgStudents.ItemsSource = StudentsRepository.GetAll();


            var dict = new Dictionary<Guid, Student>();
            var list = new List<Student>();

    
            foreach (var item in dict)
            {
                var key = item.Key;
                var Student = item.Value;
                Console.WriteLine(Student.Name);
            }

            foreach (var Student in list)
            {
       
                Console.WriteLine(Student.Name);
            }

        }

        public void Clear()
        {
            SelectedStudent = null;
        }

        public void Create()
        {
            var Student = new Student
            {
                Name = TbName.Text,
                Email = TbEmail.Text,
                Dni = TbDni.Text
            };

            var addResult = StudentsRepository.Add(Student);
            if (addResult != StudentValidationsTypes.Ok)
                MessageBox.Show($"error adding Student:{addResult}");
            else
                DgStudents.ItemsSource = StudentsRepository.GetAll();

            Clear();
        }

        public void Retrieve()
        {

            var allStudents = StudentsRepository.GetAll();

            var idAleatorio = Guid.NewGuid();
            var Student = StudentsRepository.Get(idAleatorio);

            var Student2 = StudentsRepository.GetByDni("12345678a");

        }

        public void Update()
        {
            if (SelectedStudent != null)
            {
                var StudentCopy = SelectedStudent.Clone();

                StudentCopy.Dni = TbDni.Text;
                StudentCopy.Name = TbName.Text;
                StudentCopy.Email = TbEmail.Text;

                var editResult = StudentsRepository.Update(StudentCopy);
                if (editResult != StudentValidationsTypes.Ok)
                    MessageBox.Show($"error editing Student:{editResult}");
                else
                    DgStudents.ItemsSource = StudentsRepository.GetAll();
            }
        }

        public void Delete(Student Student)
        {
            if (Student != null)
            {
                var deleteResult = StudentsRepository.Delete(Student.Id);
                if (deleteResult != StudentValidationsTypes.Ok)
                    MessageBox.Show($"error deleting Student:{deleteResult}");
                else
                    DgStudents.ItemsSource = StudentsRepository.GetAll();
            }
        }

        private void BtAdd_Click(object sender, RoutedEventArgs e)
        {
            Create();
        }

        private void BtSelect_Click(object sender, RoutedEventArgs e)
        {
            SelectedStudent = ((Button)sender).DataContext as Student;
        }

        private void BtClear_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void BtSave_Click(object sender, RoutedEventArgs e)
        {
            Update();
            SelectedStudent = null;
        }

        private void BtDelete_Click(object sender, RoutedEventArgs e)
        {
            var selectedStudent = ((Button)sender).DataContext as Student;
            Delete(selectedStudent);
        }
    }
}
