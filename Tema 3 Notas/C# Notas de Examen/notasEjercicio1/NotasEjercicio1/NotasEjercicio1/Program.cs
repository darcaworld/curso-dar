﻿using System;

namespace NotasEjercicio1
{
    class Program
    {
        static string SalirDelSistema = "Salir";
        static void Main(string[] args)
        {
            Console.WriteLine("Programa de Gestión de Notas");
            Console.WriteLine("Introduzca las Notas de examen");

            var notasDeAlumno = new List<double>();
            var keepdoing = true;

            while (keepdoing)
            {
                Console.WriteLine($"nota del alumno {notasDeAlumno.Count + 1}:");
                var notaText = Console.ReadLine();

                if(notaText == SalirDelSistema)
                {
                 keepdoing = false;
                }
                else
                {
                    var nota = 0.0;

                    if (double.TryParse(notaText.Replace(".", ","), out nota))

                    {
                        notasDeAlumno.Add(nota);
                    }
                    else
                    {
                        Console.WriteLine("Datos incorrectos");
                    }
                }   
            }

            var suma = 0.0;
            for(var i = 0; i < notasDeAlumno.Count; i++) 
            {
                suma +=  notasDeAlumno[i];
            }
            var average = suma/notasDeAlumno.Count;
            Console.WriteLine("La media de la clase es: {0}", average);
        }
    }
}
