﻿using System;
namespace notasDeExamen
{
    class Program
    {
        static string Exit = "Salir del Sistema";
        static void Main(string[] args)
        {
            Console.WriteLine("Notas de Alumnos- Ejercicio");
            Console.WriteLine("Introduzca las notas");

            var notasDeExamen = new double[11];

            notasDeExamen[0] = (4);
            notasDeExamen[1] = (5);
            notasDeExamen[2] = (6.5);
            notasDeExamen[3] = (7.8);
            notasDeExamen[4] = (5);
            notasDeExamen[5] = (6);
            notasDeExamen[6] = (8.8);
            notasDeExamen[7] = (9);
            notasDeExamen[8] = (6.5);
            notasDeExamen[9] = (10);
            notasDeExamen[10] = (9);

            var suma = notasDeExamen[0] +
                       notasDeExamen[1] +
                       notasDeExamen[2] +
                       notasDeExamen[3] +
                       notasDeExamen[4] +
                       notasDeExamen[5] +
                       notasDeExamen[6] +
                       notasDeExamen[7] +
                       notasDeExamen[8] +
                       notasDeExamen[9] +
                       notasDeExamen[10];

            var media = suma / notasDeExamen.Length;
            Console.WriteLine($"The Media about as {media}");

            var max = notasDeExamen[0];

            if (notasDeExamen[1] >max)
                max = notasDeExamen[1];

            if (notasDeExamen[2] > max)
                max = notasDeExamen[2];

            if (notasDeExamen[3] > max)
                max = notasDeExamen[3];

            if (notasDeExamen[4] > max)
                max = notasDeExamen[4];

            if (notasDeExamen[5] > max)
                max = notasDeExamen[5];

            if (notasDeExamen[6] > max)
                max = notasDeExamen[6];

            if (notasDeExamen[7] > max)
                max = notasDeExamen[7];

            if (notasDeExamen[8] > max)
                max = notasDeExamen[8];

            if (notasDeExamen[9] > max)
                max = notasDeExamen[9];

            if (notasDeExamen[10] > max)
                max = notasDeExamen[10];

            Console.WriteLine($"The Max about as {max}");


            var mim = notasDeExamen[0];

            if (notasDeExamen[1] < mim)
                mim = notasDeExamen[1];

            if (notasDeExamen[2] < mim)
                mim = notasDeExamen[2];

            if (notasDeExamen[3] < mim)
                mim = notasDeExamen[3];

            if (notasDeExamen[4] < mim)
                mim = notasDeExamen[4];

            if (notasDeExamen[5] < mim)
                mim = notasDeExamen[5];

            if (notasDeExamen[6] < mim)
                mim = notasDeExamen[6];

            if (notasDeExamen[7] < mim)
                mim = notasDeExamen[7];

            if (notasDeExamen[8] < mim)
                mim = notasDeExamen[8];

            if (notasDeExamen[9] < mim)
                mim = notasDeExamen[9];

            if (notasDeExamen[10] < mim)
                mim = notasDeExamen[10];

            Console.WriteLine($"The Max about as {mim}");
        }
       
    }

}
namespace notasDeExamen2
{
    class program
    {
        static void Maid(string[] args)
        {
            var notaDeJose = 10.00;
            var notaDeMaria = 9.50;

            var n = 100;

            var notas = new double[n];

            notas[0] = 10.00;
            notas[1] = 8.00;

            notas[n] = 5.42;

            var diccionario = new Dictionary<string, int>();
            diccionario.Add("A1", 100);

            if (!diccionario.ContainsKey("A2"))
            {
                diccionario.Add("A2", 300);
            }
            else { }

            foreach(var par in diccionario)
            {
                Console.WriteLine($"La clave {par.Key} tiene como valor {par.Value}");
            }

            foreach (var valor in diccionario.Values)
            {
                Console.WriteLine($"{valor}");            
            }
        }

    }

}