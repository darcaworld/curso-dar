﻿using System;

namespace NotaFinalExamen.Lib.Models
{
    public class Subject : Entity
    {
        public string Name { get; set; }

        public string Profesor {  get; set; }
    }
}
