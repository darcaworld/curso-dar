﻿using System;

namespace NotaFinalExamen.Lib.Models
{
    public class Exam : Entity
    {
        public Alumno Alumno {  get; set; }
        public Subject Subject {  get; set; }
        public double FinalCurso {  get; set; }
        public  DateTime TimeStamp {  get; set; }
    }
}
