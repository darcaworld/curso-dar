﻿using System.Collections.Generic;

namespace NotaFinalExamen.Lib.Models
{
    public class Alumno : Entity
    {
        public string Name {  get; set; }  
        public string DNI  {  get; set; }
        
        public List<Exam> Exam {  get; set;}

        public Alumno()
        {
          Exam = new List<Exam>();
        }

        public bool AddExam (Exam exam)
        {

            exam.Alumno = this;
            Exam.Add(exam);

            return true;

        }
    }
}
