﻿using System;
using System.Collections.Generic;

namespace NotaFinalExamen
{
    class Program
    {
        //static List<double>FinalCurso {  get; set; }

        public static Dictionary<string, Student> Alumno = new Dictionary<string, Student>();
       

        //public char KeyChar { get; set; }

        static string salir = "Salir";

        static void Main(string[] args)
        {
            Console.WriteLine("Bienvenido al programa de alumnos 2021");
            Console.WriteLine("Para ir a la gestióm de alumnos use  la opción A");
            Console.WriteLine("Para obtener información estadística use la opción E");
            Console.WriteLine("Para cerrar el sistema escriba la palabra: Salir");

            //FinalCurso = new List<double>();
            var keepdoing = true;

            while (keepdoing)
            {
                var option = Console.ReadKey().KeyChar;

                if (option == 'A')
                {
                    ShowStudentsMenu();
                }
                else if (option == 'E')
                {
                    ShowStatsMenu();
                }
            }

        }

       static void ShowStudentsMenu()
        {
            Console.WriteLine();
            ShowStudentsMenuOptions ();

            //Console.WriteLine();
            //Console.WriteLine("--Notas de Alumnos--");

            //Console.WriteLine("Para ver todos los Alumnos escriba all");
            //Console.WriteLine("Para añadir un nuevo Alumno escriba add");
            //Console.WriteLine("Para editar un  Alumno escriba edit + DNI");
            //Console.WriteLine("Para borrar un  Alumno escriba delite + DNI");
            //Console.WriteLine( "Para volver al menú principal escriba M");

            var keepdoing = true;
            while (keepdoing)
            {
                var text = Console.ReadLine();

                switch (text)
                {
                    case "all":
                        ShowAllFinalcurso();
                        break;

                    case "m":
                        keepdoing = false;
                        break;
                    default:
                        AddFinalcurso(text);
                        break;
                }

            }
            ShowMainMenu();

        }

        private void ShowStudentsMenuOptions()
        {
            Console.WriteLine("Para ver todos los Alumnos escriba all");
            Console.WriteLine("Para añadir un nuevo Alumno escriba add");
            Console.WriteLine("Para editar un  Alumno escriba edit + DNI");
            Console.WriteLine("Para borrar un  Alumno escriba delite + DNI");
            Console.WriteLine( "Para volver al menú principal escriba M");
        }
        private static void ShowMainMenu()
        {
            Console.WriteLine("Vuelta al menú principal");
            Console.WriteLine("Para introducir notas de los alumnos use la opción a");
            Console.WriteLine("Para obtener estadísticas use la opción e");

        }

        static void AddFinalcurso(string text)
        {
            var finalcurso = 0.0;

            if (double.TryParse(text, out finalcurso))
            {

                Finalcurso.Add(finalcurso);
                Console.WriteLine("Nota introducida correctamente, añada otra nota más, pulse all para lista todas o pulse m para volver al menú principal");
            }
            else
            {
                Console.WriteLine($"La Nota introducida {text} no está en un formato correcto, vuelva a introducirla correctamente");

            }

        }

    }

}
   