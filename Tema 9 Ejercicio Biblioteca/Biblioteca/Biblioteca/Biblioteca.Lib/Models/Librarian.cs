﻿using Common.Lib.Authenticator;

namespace Biblioteca.Lib.Models
{
    public class Librarian : User
    {

        #region Static Validations

        public static bool ValidateAutor(string autor)
        {
            if (string.IsNullOrEmpty(autor.Trim()))
            {
                return false;
            }
            return true;
        }

        public static bool ValidateIdFormat(string Id)
        {
            if (string.IsNullOrEmpty(Id))
                return false;
            else if (Id.Length != 10)
                return false;
            else
                return true;

        }

        #endregion
        public enum BookValidationsTypes
        {
            Ok,
            WrongIdFormat,
            ValidateIdFormat,
            IdDuplicated,
            WrongAutorFormat,
            IdNotEmpty,
            IdEmpty,
            BookNotFound
        }
    }
}
