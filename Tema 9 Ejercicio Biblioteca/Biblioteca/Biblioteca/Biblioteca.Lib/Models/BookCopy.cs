﻿
using System;
using System.Collections.Generic;
using Common.Lib.Core;

namespace Biblioteca.Lib.Models
{
    public class BookCopy : Entity
    {
        public Guid BookId { get; set; }
        public Book Book { get; set; }


        public virtual List<Loan> Loans { get; set; }

        public string Isdn { get; set; }

        public BookCopy()
        {

        }
    }

}
