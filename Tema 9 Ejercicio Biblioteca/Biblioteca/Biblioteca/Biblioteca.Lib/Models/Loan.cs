﻿using System;
using System.Collections.Generic;
using Common.Lib.Authenticator;
using Common.Lib.Core;

namespace Biblioteca.Lib.Models
{
    public class Loan : User
    {

        public Guid BookCopyId { get; set; }
        public BookCopy BookCopy { get; set; }
        public Guid StudentId { get; set; }
        public Student Student { get; set; }

        public DateTime RequestDate { get; set; }
        public DateTime ReturnDate { get; set; }
        public Loan() 
        {
        }

    }
}
