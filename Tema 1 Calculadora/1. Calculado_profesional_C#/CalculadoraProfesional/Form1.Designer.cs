﻿namespace CalculadoraProfesional
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtResultado = new System.Windows.Forms.TextBox();
            this.ce = new System.Windows.Forms.Button();
            this.c = new System.Windows.Forms.Button();
            this.borrar = new System.Windows.Forms.Button();
            this.nueve = new System.Windows.Forms.Button();
            this.ocho = new System.Windows.Forms.Button();
            this.siete = new System.Windows.Forms.Button();
            this.seis = new System.Windows.Forms.Button();
            this.cinco = new System.Windows.Forms.Button();
            this.cuatro = new System.Windows.Forms.Button();
            this.tres = new System.Windows.Forms.Button();
            this.dos = new System.Windows.Forms.Button();
            this.uno = new System.Windows.Forms.Button();
            this.coma = new System.Windows.Forms.Button();
            this.cero = new System.Windows.Forms.Button();
            this.masDiviMenos = new System.Windows.Forms.Button();
            this.cuadrado = new System.Windows.Forms.Button();
            this.raizCuadrada = new System.Windows.Forms.Button();
            this.multiplicar = new System.Windows.Forms.Button();
            this.dividir = new System.Windows.Forms.Button();
            this.restar = new System.Windows.Forms.Button();
            this.sumar = new System.Windows.Forms.Button();
            this.igual = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtResultado
            // 
            this.txtResultado.BackColor = System.Drawing.SystemColors.InfoText;
            this.txtResultado.Font = new System.Drawing.Font("Arial", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResultado.ForeColor = System.Drawing.Color.White;
            this.txtResultado.Location = new System.Drawing.Point(12, 12);
            this.txtResultado.MaxLength = 21;
            this.txtResultado.Multiline = true;
            this.txtResultado.Name = "txtResultado";
            this.txtResultado.ReadOnly = true;
            this.txtResultado.Size = new System.Drawing.Size(480, 64);
            this.txtResultado.TabIndex = 0;
            this.txtResultado.Text = "0";
            this.txtResultado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // ce
            // 
            this.ce.Font = new System.Drawing.Font("Arial Black", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ce.Location = new System.Drawing.Point(12, 99);
            this.ce.Name = "ce";
            this.ce.Size = new System.Drawing.Size(75, 65);
            this.ce.TabIndex = 1;
            this.ce.Text = "CE";
            this.ce.UseVisualStyleBackColor = true;
            this.ce.Click += new System.EventHandler(this.ce_Click);
            // 
            // c
            // 
            this.c.Font = new System.Drawing.Font("Arial Black", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c.Location = new System.Drawing.Point(107, 99);
            this.c.Name = "c";
            this.c.Size = new System.Drawing.Size(75, 65);
            this.c.TabIndex = 2;
            this.c.Text = "C";
            this.c.UseVisualStyleBackColor = true;
            this.c.Click += new System.EventHandler(this.c_Click);
            // 
            // borrar
            // 
            this.borrar.Font = new System.Drawing.Font("Arial Black", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.borrar.Location = new System.Drawing.Point(206, 99);
            this.borrar.Name = "borrar";
            this.borrar.Size = new System.Drawing.Size(75, 65);
            this.borrar.TabIndex = 3;
            this.borrar.Text = "<-";
            this.borrar.UseVisualStyleBackColor = true;
            this.borrar.Click += new System.EventHandler(this.borrar_Click);
            // 
            // nueve
            // 
            this.nueve.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nueve.Location = new System.Drawing.Point(206, 186);
            this.nueve.Name = "nueve";
            this.nueve.Size = new System.Drawing.Size(75, 65);
            this.nueve.TabIndex = 6;
            this.nueve.Text = "9";
            this.nueve.UseVisualStyleBackColor = true;
            this.nueve.Click += new System.EventHandler(this.agregarNumero);
            // 
            // ocho
            // 
            this.ocho.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ocho.Location = new System.Drawing.Point(107, 186);
            this.ocho.Name = "ocho";
            this.ocho.Size = new System.Drawing.Size(75, 65);
            this.ocho.TabIndex = 5;
            this.ocho.Text = "8";
            this.ocho.UseVisualStyleBackColor = true;
            this.ocho.Click += new System.EventHandler(this.agregarNumero);
            // 
            // siete
            // 
            this.siete.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.siete.Location = new System.Drawing.Point(12, 186);
            this.siete.Name = "siete";
            this.siete.Size = new System.Drawing.Size(75, 65);
            this.siete.TabIndex = 4;
            this.siete.Text = "7";
            this.siete.UseVisualStyleBackColor = true;
            this.siete.Click += new System.EventHandler(this.agregarNumero);
            // 
            // seis
            // 
            this.seis.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seis.Location = new System.Drawing.Point(206, 274);
            this.seis.Name = "seis";
            this.seis.Size = new System.Drawing.Size(75, 65);
            this.seis.TabIndex = 9;
            this.seis.Text = "6";
            this.seis.UseVisualStyleBackColor = true;
            this.seis.Click += new System.EventHandler(this.agregarNumero);
            // 
            // cinco
            // 
            this.cinco.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cinco.Location = new System.Drawing.Point(107, 274);
            this.cinco.Name = "cinco";
            this.cinco.Size = new System.Drawing.Size(75, 65);
            this.cinco.TabIndex = 8;
            this.cinco.Text = "5";
            this.cinco.UseVisualStyleBackColor = true;
            this.cinco.Click += new System.EventHandler(this.agregarNumero);
            // 
            // cuatro
            // 
            this.cuatro.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cuatro.Location = new System.Drawing.Point(12, 274);
            this.cuatro.Name = "cuatro";
            this.cuatro.Size = new System.Drawing.Size(75, 65);
            this.cuatro.TabIndex = 7;
            this.cuatro.Text = "4";
            this.cuatro.UseVisualStyleBackColor = true;
            this.cuatro.Click += new System.EventHandler(this.agregarNumero);
            // 
            // tres
            // 
            this.tres.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tres.Location = new System.Drawing.Point(206, 360);
            this.tres.Name = "tres";
            this.tres.Size = new System.Drawing.Size(75, 65);
            this.tres.TabIndex = 12;
            this.tres.Text = "3";
            this.tres.UseVisualStyleBackColor = true;
            this.tres.Click += new System.EventHandler(this.agregarNumero);
            // 
            // dos
            // 
            this.dos.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dos.Location = new System.Drawing.Point(107, 360);
            this.dos.Name = "dos";
            this.dos.Size = new System.Drawing.Size(75, 65);
            this.dos.TabIndex = 11;
            this.dos.Text = "2";
            this.dos.UseVisualStyleBackColor = true;
            this.dos.Click += new System.EventHandler(this.agregarNumero);
            // 
            // uno
            // 
            this.uno.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uno.Location = new System.Drawing.Point(12, 360);
            this.uno.Name = "uno";
            this.uno.Size = new System.Drawing.Size(75, 65);
            this.uno.TabIndex = 10;
            this.uno.Text = "1";
            this.uno.UseVisualStyleBackColor = true;
            this.uno.Click += new System.EventHandler(this.agregarNumero);
            // 
            // coma
            // 
            this.coma.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.coma.Location = new System.Drawing.Point(206, 445);
            this.coma.Name = "coma";
            this.coma.Size = new System.Drawing.Size(75, 65);
            this.coma.TabIndex = 15;
            this.coma.Text = ",";
            this.coma.UseVisualStyleBackColor = true;
            this.coma.Click += new System.EventHandler(this.coma_Click);
            // 
            // cero
            // 
            this.cero.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cero.Location = new System.Drawing.Point(107, 445);
            this.cero.Name = "cero";
            this.cero.Size = new System.Drawing.Size(75, 65);
            this.cero.TabIndex = 14;
            this.cero.Text = "0";
            this.cero.UseVisualStyleBackColor = true;
            this.cero.Click += new System.EventHandler(this.agregarNumero);
            // 
            // masDiviMenos
            // 
            this.masDiviMenos.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.masDiviMenos.Location = new System.Drawing.Point(12, 445);
            this.masDiviMenos.Name = "masDiviMenos";
            this.masDiviMenos.Size = new System.Drawing.Size(75, 65);
            this.masDiviMenos.TabIndex = 13;
            this.masDiviMenos.Text = "+/-";
            this.masDiviMenos.UseVisualStyleBackColor = true;
            this.masDiviMenos.Click += new System.EventHandler(this.masDiviMenos_Click);
            // 
            // cuadrado
            // 
            this.cuadrado.Font = new System.Drawing.Font("Arial Black", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cuadrado.Location = new System.Drawing.Point(417, 99);
            this.cuadrado.Name = "cuadrado";
            this.cuadrado.Size = new System.Drawing.Size(75, 65);
            this.cuadrado.TabIndex = 17;
            this.cuadrado.Tag = "²";
            this.cuadrado.Text = "x²";
            this.cuadrado.UseVisualStyleBackColor = true;
            this.cuadrado.Click += new System.EventHandler(this.clickOperador);
            // 
            // raizCuadrada
            // 
            this.raizCuadrada.Font = new System.Drawing.Font("Arial Black", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.raizCuadrada.Location = new System.Drawing.Point(318, 99);
            this.raizCuadrada.Name = "raizCuadrada";
            this.raizCuadrada.Size = new System.Drawing.Size(75, 65);
            this.raizCuadrada.TabIndex = 16;
            this.raizCuadrada.Tag = "√";
            this.raizCuadrada.Text = "√";
            this.raizCuadrada.UseVisualStyleBackColor = true;
            this.raizCuadrada.Click += new System.EventHandler(this.clickOperador);
            // 
            // multiplicar
            // 
            this.multiplicar.Font = new System.Drawing.Font("Arial Black", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.multiplicar.Location = new System.Drawing.Point(417, 186);
            this.multiplicar.Name = "multiplicar";
            this.multiplicar.Size = new System.Drawing.Size(75, 65);
            this.multiplicar.TabIndex = 19;
            this.multiplicar.Tag = "*";
            this.multiplicar.Text = "*";
            this.multiplicar.UseVisualStyleBackColor = true;
            this.multiplicar.Click += new System.EventHandler(this.clickOperador);
            // 
            // dividir
            // 
            this.dividir.Font = new System.Drawing.Font("Arial Black", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dividir.Location = new System.Drawing.Point(318, 186);
            this.dividir.Name = "dividir";
            this.dividir.Size = new System.Drawing.Size(75, 65);
            this.dividir.TabIndex = 18;
            this.dividir.Tag = "/";
            this.dividir.Text = "/";
            this.dividir.UseVisualStyleBackColor = true;
            this.dividir.Click += new System.EventHandler(this.clickOperador);
            // 
            // restar
            // 
            this.restar.Font = new System.Drawing.Font("Arial Black", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.restar.Location = new System.Drawing.Point(417, 274);
            this.restar.Name = "restar";
            this.restar.Size = new System.Drawing.Size(75, 65);
            this.restar.TabIndex = 21;
            this.restar.Tag = "-";
            this.restar.Text = "-";
            this.restar.UseVisualStyleBackColor = true;
            this.restar.Click += new System.EventHandler(this.clickOperador);
            // 
            // sumar
            // 
            this.sumar.Font = new System.Drawing.Font("Arial Black", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sumar.Location = new System.Drawing.Point(318, 274);
            this.sumar.Name = "sumar";
            this.sumar.Size = new System.Drawing.Size(75, 65);
            this.sumar.TabIndex = 20;
            this.sumar.Tag = "+";
            this.sumar.Text = "+";
            this.sumar.UseVisualStyleBackColor = true;
            this.sumar.Click += new System.EventHandler(this.clickOperador);
            // 
            // igual
            // 
            this.igual.Font = new System.Drawing.Font("Arial Black", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.igual.Location = new System.Drawing.Point(318, 360);
            this.igual.Name = "igual";
            this.igual.Size = new System.Drawing.Size(174, 150);
            this.igual.TabIndex = 22;
            this.igual.Tag = "=";
            this.igual.Text = "=";
            this.igual.UseVisualStyleBackColor = true;
            this.igual.Click += new System.EventHandler(this.igual_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(504, 531);
            this.Controls.Add(this.igual);
            this.Controls.Add(this.restar);
            this.Controls.Add(this.sumar);
            this.Controls.Add(this.multiplicar);
            this.Controls.Add(this.dividir);
            this.Controls.Add(this.cuadrado);
            this.Controls.Add(this.raizCuadrada);
            this.Controls.Add(this.coma);
            this.Controls.Add(this.cero);
            this.Controls.Add(this.masDiviMenos);
            this.Controls.Add(this.tres);
            this.Controls.Add(this.dos);
            this.Controls.Add(this.uno);
            this.Controls.Add(this.seis);
            this.Controls.Add(this.cinco);
            this.Controls.Add(this.cuatro);
            this.Controls.Add(this.nueve);
            this.Controls.Add(this.ocho);
            this.Controls.Add(this.siete);
            this.Controls.Add(this.borrar);
            this.Controls.Add(this.c);
            this.Controls.Add(this.ce);
            this.Controls.Add(this.txtResultado);
            this.Name = "Form1";
            this.Text = "CalculadoraProfesional";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtResultado;
        private System.Windows.Forms.Button ce;
        private System.Windows.Forms.Button c;
        private System.Windows.Forms.Button borrar;
        private System.Windows.Forms.Button nueve;
        private System.Windows.Forms.Button ocho;
        private System.Windows.Forms.Button siete;
        private System.Windows.Forms.Button seis;
        private System.Windows.Forms.Button cinco;
        private System.Windows.Forms.Button cuatro;
        private System.Windows.Forms.Button tres;
        private System.Windows.Forms.Button dos;
        private System.Windows.Forms.Button uno;
        private System.Windows.Forms.Button coma;
        private System.Windows.Forms.Button cero;
        private System.Windows.Forms.Button masDiviMenos;
        private System.Windows.Forms.Button cuadrado;
        private System.Windows.Forms.Button raizCuadrada;
        private System.Windows.Forms.Button multiplicar;
        private System.Windows.Forms.Button dividir;
        private System.Windows.Forms.Button restar;
        private System.Windows.Forms.Button sumar;
        private System.Windows.Forms.Button igual;
    }
}

