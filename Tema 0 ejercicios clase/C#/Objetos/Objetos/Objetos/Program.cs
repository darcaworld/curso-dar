﻿using System;

namespace Objetos
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Clse de Objetos");

            var alta = new alumno();
            {
               var name = "Dar";
               var year = 1999;
               var email = "ex@ex.com";
            }

            DatosDelAlumnoCursoDeFSD(alta); 

            Console.WriteLine();

        }
        static void DatosDelAlumnoCursoDeFSD(alumno alumno)
        {
            Console.WriteLine("función estática: Alumno" + alumno.name+ "Año de Nacimiento" + alumno.year.ToString() + 
                "email de contacto" + alumno.email);
        }

    }
    class alumno
    {

        public string name { get; set; }
        public int year { get; set; }
        public string email { get; set; }

        public alumno()
        {

        }
        public alumno(string name, int year, string email)
        {
            this.name = name;
            this.year = year;
            this.email = email;
        }
         public void ShowInfo()
        {
            Console.WriteLine("función estática: Alumno" + this.name + "Año de Nacimiento" + this.year.ToString() +
                "email de contacto" + this.email);
        }
    }
}