﻿using System;

using System.Collections.Generic;

namespace ListaBucles
{

    class program

    {
        static void Main(string [] args)
        {
            Console.WriteLine("Ejercicio  Lista de la Compra!");

            var lista = new List<string>();

            lista.Add (" arroz");
            lista.Add (" pasta");
            lista.Add (" galletas");
            lista.Add (" huevos");
            lista.Add (" carne");

            for (var i = 0; i < lista.Count; i++)
            {
                var palabra = lista[i];
                Console.WriteLine($"La palabra en la posición {i} es" + palabra);

            }

            var condition = true;
            var j = 0;

            while (condition)
            {
                var palabra = lista[j];

                if (palabra.Contains("y"))
                {
                    condition = false;
                    Console.WriteLine($"hemos encontrado la letra prohibida en la palabra {palabra} en la `posición {j}");
                }
                else
                {
                    Console.WriteLine($"La palabra en la posición {j} es" + palabra);
                    j++;
                }

                if (j == lista.Count)
                    condition = false;
            }            
        }

    }

}