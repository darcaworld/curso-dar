﻿using Microsoft.AspNetCore.Mvc;
using Tets.web.api.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Tets.web.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        // GET: api/<StudentsController>
        [HttpGet]
        public IEnumerable<Student> Get()
        {
            var st1 = new Student()
            {
                Id = Guid.NewGuid(),
                Name = "Josep",
                Email = "Josep@pepe.com",
                Password = "1234A"
            };
            
            var st2 = new Student()
            {
                Id = Guid.NewGuid(),
                Name = "pepe",
                Email = "pepe@pepe.com",
                Password = "1234B"
            };

            var output = new List<Student>();
            output.Add(st1);
            output.Add(st2);

            return output;
        }

        // GET api/<StudentsController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<StudentsController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<StudentsController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<StudentsController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
