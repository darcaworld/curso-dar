﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.Lib.Models;

namespace WpfApp1.Lib.Models
{
        public class Subject : Entity
    {
            public string Name { get; set; }

            public string Code { get; set; }
            public ICollection<Enrollment> Enrollments { get; set; }
        }
}
