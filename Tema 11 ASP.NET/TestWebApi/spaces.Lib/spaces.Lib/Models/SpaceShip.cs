﻿using System;


namespace spaces.Lib.Models
{
    public class SpaceShip
    {
        public Guid Id { get; set; }
        public string Brand { get; set; }

        public string Password { get; set; }

        public int Factor { get; set; }

        public string Color { get; set; }

        public int PassengerCapacity { get; set; }


    }
}
