class SpaceShips
{
    Brand = "MAQUINA 1";
    Password = "12345678"
    Factor = 2;
    Color = "Black";
    PassengerCapacity= 400;

    Http = null;

    Items = [];

    GridOptions = 
    {
        columnDefs : 
        [ 
                { name: 'Brand', field: 'brand'},
                { name: 'Factor', field: 'factor'},
                { name: 'Color', field: 'color'},
                { name: 'PassengerCapacity', field: 'passengerCapacity'},
        ],
        data : []
      };

    
    constructor($http)
    {        
        this.Http = $http;
        this.GetAll();
    }

    GetAll()
    {
        var getReq = 
        {
            method: 'GET',            
            url: 'api/SpaceShips'            
        };

        this.Http(getReq).then(
            (response) =>
            {
                 this.LoadShips(response.data);
            },
            (error) => alert(error.statusText));                
    }

    LoadShips(ships)
    {
        this.GridOptions.data.length = 0; 

        for (let i in ships)
            this.GridOptions.data.push(ships[i]);
    }

    Add()
    {
        var spaceShip = new SpaceShip();
        
        spaceShip.Brand = this.Brand;
        spaceShip.Password = this.Password;
        spaceShip.Factor = this.Factor;
        spaceShip.Color = this.Color;
        spaceShip.PassengerCapacity = this.PassengerCapacity;

        var postRequest = 
        {
            method: 'POST',
            url: 'api/SpaceShips',
            headers: { 'Content-Type': 'application/json' },
            data: spaceShip
        };

        this.Http(postRequest).then(
               (response) => this.#OnAddSuccess(response.data),            
               (error) => alert("server error: " + error.statusText)); 
    }     

        #OnAddSuccess(result)
        {        
            if (result == true)
            {
                alert("se ha añadido correctamente la nave");
            }
            else
            {
                alert("no ha añadido correctamente la nave");
            }

            this.GetAll();        
        }

        Delete()
    {
        var spaceShip = new SpaceShip();
        
        spaceShip.Brand = this.Brand;
        spaceShip.Password = this.Password;
        spaceShip.Factor = this.Factor;
        spaceShip.Color = this.Color;
        spaceShip.PassengerCapacity = this.PassengerCapacity;

        var postRequest = 
        {
            method: 'POST',
            url: 'api/SpaceShips',
            headers: { 'Content-Type': 'application/json' },
            data: SpaceShip
        };

        this.Http(postRequest).then(
               (response) => this.#OnAddSuccess(response.data),            
               (error) => alert("server error: " + error.statusText)); 

    }
}

App.
  component('spaceships', {   
    templateUrl: 'scripts/views/spaceships/spaceships.html',
    controller: SpaceShips,
    controllerAs: "vm"
  });