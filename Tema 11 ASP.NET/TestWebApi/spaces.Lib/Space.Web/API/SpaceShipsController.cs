﻿using Microsoft.AspNetCore.Mvc;
using Space.Web.DAL;
using spaces.Lib.Models;
using System.Linq;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Spaces.Web.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpaceShipsController : ControllerBase
    {
       
        [HttpGet]
        public IEnumerable<SpaceShip> Get()
        {
            var DbContext = new SpaceDbContex();

            if (DbContext.SpacesShips.Count() == 0)
            {
                var nave1 = new SpaceShip()
                {
                    Id = Guid.NewGuid(),
                    Brand = "Omega",
                    Password = "Omega123456789",
                    Factor = 20,
                    Color = "Pink",
                    PassengerCapacity = 50,
                };

                var nave2 = new SpaceShip()
                {
                    Id = Guid.NewGuid(),
                    Brand = "Alpha",
                    Password = "Alpha123456789",
                    Factor = 200,
                    Color = "Red",
                    PassengerCapacity = 500,
                };

                DbContext.SpacesShips.Add(nave1);
                DbContext.SpacesShips.Add(nave1);

                DbContext.SaveChanges();


            }

            return DbContext.SpacesShips.ToList();


        }

  
        [HttpGet("{id}")]
        public  string Get(int id)
        {
            return "value";
        }

        [HttpPost]
        public bool Post([FromBody] SpaceShip item)
        {
            using(var dbContext = new SpaceDbContex())
            {
                if (item.Id != default && dbContext.SpacesShips.Any(x => x.Id == item.Id))
                {
                    return false;
                }
                if (item.Id == default)
                    item.Id = Guid.NewGuid();

                dbContext.SpacesShips.Add(item);
                dbContext.SaveChanges();
                return true;
            }
        }
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
