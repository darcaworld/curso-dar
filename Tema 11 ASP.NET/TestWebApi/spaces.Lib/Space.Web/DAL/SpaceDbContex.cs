﻿using Microsoft.EntityFrameworkCore;
using spaces.Lib.Models;


namespace Space.Web.DAL
{
    public class SpaceDbContex : DbContext
    {
        public DbSet<SpaceShip> SpacesShips { get; set; }

        public SpaceDbContex () : base ()
        {


        }

        protected override void OnModelCreating(ModelBuilder Builder)
        {
            Builder.Entity<SpaceShip>().ToTable("SpaceShips");

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(@"Server=LAPTOP-TSPE80VL;Database=Academy;Trusted_Connection=True;MultipleActiveResultSets=true");
            optionsBuilder
                .UseMySql(connectionString: @"server=localhost;database=spaceships;uid=DPARRA;password=12345678;",
                new MySqlServerVersion(new Version(8, 0, 23)));
        }


    };
}
